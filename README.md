# Challenge Martian Robots

### Pasos para levantar el proyecto 
El proyecto se realizó con spring boot y maven como manejador de paquetes.

mvn spring-boot:run 


### Probar el servicio
Se documentó el proyecto con swagger ui

http://localhost:8080/swagger-ui.html

El input del servicio es un file que contiene la cadena de instrucciones, 
se agregó al proyecto para comodidad de las pruebas

instructions.txt
