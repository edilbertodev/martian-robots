package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.exception.FileInvalidException;
import mx.challenge.draftea.martian.robots.model.FileContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Servicio para procesar y calcular la posicion final de los robots
 *
 * @author Edilberto Ventura
 */
@Service
public class MartianRobotService implements IMartianRobotService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MartianRobotService.class);

    @Autowired
    private IFileService fileService;

    @Autowired
    private IRobotService robotService;

    /**
     * Obtiene la posición final de los robots
     *
     * @param file Información de los robots
     * @return Lista de posiciones finales
     * @throws FileInvalidException Excepción si el archivo es inválido
     */
    @Override
    public List<String> processInstructions(final MultipartFile file) throws FileInvalidException {
        List<String> finalPositions = new ArrayList<>();
        fileService.validateTypeFile(file.getContentType());
        FileContent fileContent = fileService.getContent(file);

        LOGGER.info("Inicia el cálculo de las posiciones finales de los robots");
        fileContent.getLstRobots().forEach(robot -> {
            String finalPosition = robotService.move(robot, fileContent.getLimit());
            finalPositions.add(finalPosition);
            LOGGER.info("==========================================");
            LOGGER.info("==========================================");

        });

        return finalPositions;
    }
}
