package mx.challenge.draftea.martian.robots.model;

import mx.challenge.draftea.martian.robots.util.DirectionEnum;

import java.util.List;

/**
 * Modelado del robot
 *
 * @author Edilberto Ventura
 */
public class Robot {
    private Coordenates coordenates;

    private DirectionEnum direction;

    private List<String> instructions;

    public Robot() {
    }

    public Robot(Coordenates coordenates, DirectionEnum direction) {
        this.coordenates = coordenates;
        this.direction = direction;
    }

    public Coordenates getCoordenates() {
        return coordenates;
    }

    public void setCoordenates(Coordenates coordenates) {
        this.coordenates = coordenates;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

    public void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }

    public List<String> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<String> instructions) {
        this.instructions = instructions;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "coordenates=" + coordenates +
                ", direction=" + direction +
                ", instructions=" + instructions +
                '}';
    }
}
