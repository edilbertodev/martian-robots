package mx.challenge.draftea.martian.robots.exception;

/**
 * Excepcion cuando:
 * - las coordenadas exceden el limite permitido, especificado en los properties
 * - las coordenadas no son numéricas
 *
 * @author Edilberto Ventura
 */
public class CoordinateInvalidException extends Exception {
    public CoordinateInvalidException(final String message) {
        super(message);
    }
}
