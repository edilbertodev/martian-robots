package mx.challenge.draftea.martian.robots.model;

import java.util.List;

/**
 * POJO para mapear el contenido del archivo
 *
 * @author Edilberto Ventura
 */
public class FileContent {
    private Coordenates limit;

    private List<Robot> lstRobots;

    public Coordenates getLimit() {
        return limit;
    }

    public void setLimit(Coordenates limit) {
        this.limit = limit;
    }

    public List<Robot> getLstRobots() {
        return lstRobots;
    }

    public void setLstRobots(List<Robot> lstRobots) {
        this.lstRobots = lstRobots;
    }

    @Override
    public String toString() {
        return "FileContent{" +
                "limit=" + limit +
                ", lstRobots=" + lstRobots +
                '}';
    }
}
