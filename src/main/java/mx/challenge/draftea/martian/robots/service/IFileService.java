package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.exception.FileInvalidException;
import mx.challenge.draftea.martian.robots.model.FileContent;
import org.springframework.web.multipart.MultipartFile;

public interface IFileService {

    void validateTypeFile(final String contentType) throws FileInvalidException;

    FileContent getContent(final MultipartFile file) throws FileInvalidException;
}
