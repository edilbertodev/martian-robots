package mx.challenge.draftea.martian.robots.util;

/**
 * Direcciones permitidas
 *
 * @author Edilberto Ventura
 */
public enum DirectionEnum {
    NORT("N"),
    SOUTH("S"),
    EAST("E"),
    WEST("W");

    private String name;

    DirectionEnum(String name) {
        this.name = name;
    }

    public String getAlias() {
        return name;
    }

    public static DirectionEnum getEnum(String name) {
        switch (name) {
            case "N":
                return NORT;
            case "S":
                return SOUTH;
            case "E":
                return EAST;
            case "W":
                return WEST;
            default:
                return null;
        }
    }
}
