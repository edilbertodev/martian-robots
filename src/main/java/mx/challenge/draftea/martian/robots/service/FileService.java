package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.exception.CoordinateInvalidException;
import mx.challenge.draftea.martian.robots.exception.FileInvalidException;
import mx.challenge.draftea.martian.robots.exception.InstructionInvalidException;
import mx.challenge.draftea.martian.robots.model.Coordenates;
import mx.challenge.draftea.martian.robots.model.FileContent;
import mx.challenge.draftea.martian.robots.model.Robot;
import mx.challenge.draftea.martian.robots.util.DirectionEnum;
import mx.challenge.draftea.martian.robots.util.InstructionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static mx.challenge.draftea.martian.robots.util.ConstantsUtil.*;

/**
 * Servicio para procesar el archivo cargado y convertirlo a POJO para el manejo en Java
 *
 * @author Edilberto Ventura
 */
@Service
public class FileService implements IFileService {

    private static final String CONTENT_TYPE_VALID = "text/plain";

    @Autowired
    private Environment environment;

    /**
     * Valida que la extensión del archivo sea el permitido para procesarlo text/plain
     *
     * @param contentType Tipo de archivo, del documento cargado
     * @throws FileInvalidException Excepción si el tipo de archivo no es válido
     */
    @Override
    public void validateTypeFile(final String contentType) throws FileInvalidException {
        if (!CONTENT_TYPE_VALID.equals(contentType)) {
            throw new FileInvalidException(ERROR_TYPE_FILE);
        }
    }

    /**
     * Convierte el contenido del archivo a pojo
     *
     * @param file File cargado
     * @return Pojo con la información del archivo
     * @throws FileInvalidException Excepción en caso de que las coordenadas no
     *                              sean validas o no se cumpla con la estructura esperada
     */
    @Override
    public FileContent getContent(final MultipartFile file) throws FileInvalidException {
        FileContent fileContent = new FileContent();
        try {
            String content = convertByteToString(file);
            String[] contentValues = content.split("\n");
            Coordenates limit = getCoordenates(contentValues[0]);
            List<String> data = Arrays.stream(contentValues).filter(valueRow -> valueRow.trim().length() != 0)
                    .collect(Collectors.toList());
            // Se quita la linea de coordenadas del cuadrante, ya se almacenaron en la variable 'limit'
            data.remove(0);
            List<Robot> lstRobots = getRobots(data);

            // Construye el objeto a partir del archivo
            fileContent.setLimit(limit);
            fileContent.setLstRobots(lstRobots);

        } catch (Exception e) {
            throw new FileInvalidException(e.getMessage());
        }

        return fileContent;
    }

    /**
     * Obtiene el contenido como cadena
     *
     * @param file File cargado
     * @return Cadena con el contenido del archivo
     * @throws IOException Exception en caso de que no se pueda convertir el contenido a cadena
     */
    private String convertByteToString(final MultipartFile file) throws IOException {
        return new String(file.getBytes());
    }

    /**
     * Obtiene la información de los robots desde el archivo
     *
     * @param data Datos del archivo
     * @return Lista de robot con sus coordenadas, dirección e instrucciones
     * @throws CoordinateInvalidException  Excepción si las coordenadas de algún robot es inválida
     * @throws InstructionInvalidException Excepción si alguna instrucción es inválida
     */
    private List<Robot> getRobots(final List<String> data) throws CoordinateInvalidException, InstructionInvalidException {
        List<Robot> lstRobots = new ArrayList<>();
        Robot robot = new Robot();
        for (int i = 0; i < data.size(); i++) {
            if (i % 2 == 0) {
                robot = getRobot(data.get(i));
            }

            if (i % 2 > 0) {
                robot.setInstructions(getInstructions(data.get(i)));
                lstRobots.add(robot);
            }
        }

        return lstRobots;
    }

    /**
     * Convierte los datos de las coordenadas y dirección en el objeto Robot
     *
     * @param row Datos del robot
     * @return Objeto robot inicializado
     * @throws CoordinateInvalidException Exception en caso que el robot se incorrecto
     */
    private Robot getRobot(final String row) throws CoordinateInvalidException {
        String[] valuesRow = row.split(" ");
        if (valuesRow.length < 3) {
            throw new CoordinateInvalidException(ERROR_COORDENATE_DIRECTION_NOT_FOUND);
        }

        Coordenates coordenates = getCoordenates(row);
        return new Robot(coordenates, DirectionEnum.getEnum(valuesRow[2]));
    }

    /**
     * Convierte la cadena de instrucciones en un lista
     *
     * @param row Cadena de instrucciones
     * @return Lista de instrucciones
     * @throws InstructionInvalidException Excepción en caso que alguna instrucción se inválida (no sea L, R o F)
     */
    private List<String> getInstructions(final String row) throws InstructionInvalidException {
        String[] valuesRow = row.trim().split("");
        if (valuesRow.length > environment.getProperty(MAX_SIZE_INSTRUCTIONS, Long.class)) {
            throw new InstructionInvalidException(ERROR_INSTRUCTIONS_LARGE);
        }

        boolean instructionInvalid = Arrays.stream(valuesRow).filter(instruction ->
                !instruction.equals(InstructionEnum.LEFT.getAlias())
                        && !instruction.equals(InstructionEnum.RIGHT.getAlias())
                        && !instruction.equals(InstructionEnum.FRONT.getAlias())
        ).count() > 0;

        if (instructionInvalid) {
            throw new InstructionInvalidException(ERROR_INSTRUCTIONS_INVALID);
        }

        return Arrays.asList(valuesRow);
    }

    /**
     * Obtiene las coordenadas del row del archivo
     *
     * @param row Cadena del archivo con las coordenadas
     * @return Coordenadas en objeto
     * @throws CoordinateInvalidException Excepcion si las coordenadas no son validas
     */
    private Coordenates getCoordenates(final String row) throws CoordinateInvalidException {
        String[] valuesRow = row.split(" ");
        // Valida que vengan ambas coordenadas
        if (valuesRow == null || valuesRow.length < 2) {
            throw new CoordinateInvalidException(ERROR_COORDENATE_NOT_FOUND);
        }

        long x = 0;
        long y = 0;

        // Valida que ambas coordenadas sean numéricas
        try {
            x = Long.parseLong(valuesRow[0]);
            y = Long.parseLong(valuesRow[1]);
        } catch (Exception e) {
            throw new CoordinateInvalidException(ERROR_COORDENATE_INVALID);
        }

        // Valida que las coordenadas no sean mayores a lo especificado
        long sizeMax = environment.getProperty(MAX_VALUE_COORDENATE, Long.class);
        if (x > sizeMax || y > sizeMax) {
            throw new CoordinateInvalidException(ERROR_COORDENATE_INVALID);
        }

        return new Coordenates(x, y);
    }
}
