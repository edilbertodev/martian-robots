package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.exception.FileInvalidException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IMartianRobotService {

    List<String> processInstructions(MultipartFile file) throws FileInvalidException, IOException;
}
