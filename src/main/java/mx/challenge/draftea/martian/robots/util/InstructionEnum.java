package mx.challenge.draftea.martian.robots.util;

/**
 * Instrucciones permitidas
 *
 * @author Edilberto Ventura
 */
public enum InstructionEnum {
    LEFT("L"),
    RIGHT("R"),
    FRONT("F");

    private String name;

    InstructionEnum(String name) {
        this.name = name;
    }

    public String getAlias() {
        return name;
    }

    public static InstructionEnum getEnum(String name) {
        switch (name) {
            case "F":
                return FRONT;
            case "R":
                return RIGHT;
            case "L":
                return LEFT;
            default:
                return null;
        }
    }
}
