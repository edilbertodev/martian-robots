package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.model.Coordenates;
import mx.challenge.draftea.martian.robots.util.ConstantsUtil;
import mx.challenge.draftea.martian.robots.util.DirectionEnum;
import mx.challenge.draftea.martian.robots.util.InstructionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static mx.challenge.draftea.martian.robots.util.DirectionEnum.*;
import static mx.challenge.draftea.martian.robots.util.InstructionEnum.LEFT;
import static mx.challenge.draftea.martian.robots.util.InstructionEnum.RIGHT;

/**
 * Servicio para el cálculo de las nuevas direcciones y de las validaciones del robot en cada instruccion
 *
 * @author Edilberto Ventura
 */
@Service
public class DirectionService implements IDirectionService {

    private Map<String, DirectionEnum> neighbour;

    private Map<DirectionEnum, Coordenates> lostCoordenates;

    @Autowired
    private Environment environment;

    @PostConstruct
    public void init() {
        lostCoordenates = new HashMap<>();
    }

    /**
     * Obtiene la nueva dirección calculada a partir de la dirección original y la instrucción L o R
     *
     * @param instruction     Instrucción recibida L o R
     * @param originDirection Dirección original del robot
     * @return Nueva dirección NORT | SOUTH | EAST | WEST
     */
    @Override
    public DirectionEnum getDirection(InstructionEnum instruction, DirectionEnum originDirection) {

        if (neighbour == null) {
            neighbour = new HashMap<>();
            neighbour.put(NORT.name().concat(LEFT.name()), WEST);
            neighbour.put(NORT.name().concat(RIGHT.name()), EAST);
            neighbour.put(SOUTH.name().concat(LEFT.name()), EAST);
            neighbour.put(SOUTH.name().concat(RIGHT.name()), WEST);
            neighbour.put(EAST.name().concat(LEFT.name()), NORT);
            neighbour.put(EAST.name().concat(RIGHT.name()), SOUTH);
            neighbour.put(WEST.name().concat(LEFT.name()), SOUTH);
            neighbour.put(WEST.name().concat(RIGHT.name()), NORT);
        }

        return neighbour.get(originDirection.name().concat(instruction.name()));
    }

    /**
     * Agrega la ubicación donde el robot sale de los limites
     *
     * @param direction   Dirección en la que avanzó el robot
     * @param coordenates Coordenada a la que avanzó cuando se perdió
     */
    @Override
    public void addLostDirection(final DirectionEnum direction, final Coordenates coordenates) {
        lostCoordenates.put(direction, coordenates);
    }

    /**
     * Valida si una coordenada ya se registró dentro de las coordenadas perdidas
     *
     * @param direction   Dirección a validar
     * @param coordenates Coordenada a validar
     * @return true -> No existe la coordenada y se registra | false -> Existe la coordenada y no se registra
     */
    @Override
    public boolean isLostCoordenate(DirectionEnum direction, Coordenates coordenates) {
        boolean isLost = false;
        Coordenates lost = lostCoordenates.get(direction);
        if (lost == null) {
            isLost = true;
            addLostDirection(direction, coordenates);
        } else if (lost.getX() != coordenates.getX() || lost.getY() != coordenates.getY()) {
            isLost = true;
            addLostDirection(direction, coordenates);
        }

        return isLost;
    }

    /**
     * Valida si las coordenadas esta fuera de los límites o es mayor a la permitida
     *
     * @param coordenates Coordenadas a validar
     * @param limit       Límite permitido
     * @return true -> Sale de los límites | false -> Está dentro de los límites
     */
    @Override
    public boolean isOutEdge(final Coordenates coordenates, final Coordenates limit) {
        long sizeMax = environment.getProperty(ConstantsUtil.MAX_VALUE_COORDENATE, Long.class);

        return coordenates.getX() > sizeMax || coordenates.getY() > sizeMax
                || coordenates.getX() > limit.getX() || coordenates.getY() > limit.getY();
    }

    /**
     * Limpia las posiciones almacenadas como perdidas, ya que se tomarán a partir del nuevo cuadrante
     */
    @Override
    public void clearLostCoordenates() {
        lostCoordenates.clear();
    }
}
