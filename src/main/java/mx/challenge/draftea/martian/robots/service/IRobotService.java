package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.model.Coordenates;
import mx.challenge.draftea.martian.robots.model.Robot;

public interface IRobotService {

    String move(final Robot robot, final Coordenates limit);
}
