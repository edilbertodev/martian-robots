package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.exception.CoordinateInvalidException;
import mx.challenge.draftea.martian.robots.model.Coordenates;
import mx.challenge.draftea.martian.robots.model.Robot;
import mx.challenge.draftea.martian.robots.util.ConstantsUtil;
import mx.challenge.draftea.martian.robots.util.DirectionEnum;
import mx.challenge.draftea.martian.robots.util.InstructionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Servicio para ejecutar las instrucciones sobre el robot
 *
 * @author Edilberto Ventura
 */
@Service
public class RobotService implements IRobotService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RobotService.class);

    @Autowired
    private Environment environment;

    @Autowired
    private IDirectionService directionService;

    @PostConstruct
    public void init() {
        directionService.clearLostCoordenates();
    }

    /**
     * Ejecuta el movimiento del robot
     *
     * @param robot Robot que se moverá
     * @param limit Limite del cuadrante
     * @return Posición final del robot
     */
    @Override
    public String move(final Robot robot, final Coordenates limit) {
        boolean lost = false;
        LOGGER.info("Robot " + robot);
        for (String intruction : robot.getInstructions()) {
            try {
                processInstruction(intruction, robot, limit);
            } catch (CoordinateInvalidException ce) {
                lost = true;
                break;
            }
        }

        return getFinalPosition(robot, lost);
    }

    /**
     * Procesa una instrucción
     *
     * @param intruction Instrucción a ejecutar
     * @param robot      Robot sobre el que se ejecuta la instrucción
     * @param limit      Limite del cuadrante
     * @throws CoordinateInvalidException Exception si se pierde el robot
     */
    private void processInstruction(final String intruction, final Robot robot, final Coordenates limit) throws CoordinateInvalidException {
        LOGGER.info("Instruccion: " + intruction);
        switch (InstructionEnum.getEnum(intruction)) {
            case LEFT:
            case RIGHT:
                DirectionEnum newDirection = rotate(InstructionEnum.getEnum(intruction), robot.getDirection());
                robot.setDirection(newDirection);
                break;
            case FRONT:
                Coordenates newCoordenates = stepForward(robot.getDirection(), robot.getCoordenates(), limit);
                robot.setCoordenates(newCoordenates);
                break;
            default:
                break;
        }
        LOGGER.info("Coordenadas: " + robot.getCoordenates() + " Direccion: " + robot.getDirection());
    }

    /**
     * Convierte a cadena la información final del robot
     *
     * @param robot Información del robot posterior a las instrucciones ejecutadas
     * @param lost  Indicador para saber si el robot se perdió o no
     * @return Cadena de la posición final del robot
     */
    private String getFinalPosition(final Robot robot, final boolean lost) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(robot.getCoordenates().getX()).append(ConstantsUtil.SPACE)
                .append(robot.getCoordenates().getY()).append(ConstantsUtil.SPACE)
                .append(robot.getDirection().getAlias());
        if (lost) {
            stringBuilder.append(ConstantsUtil.SPACE).append(ConstantsUtil.LOST);
        }

        return stringBuilder.toString();
    }

    /**
     * Gira 90 grado al robot
     *
     * @param instructionRotate Instrucción de rotación L o R
     * @param originDirection   Dirección antes de girar
     * @return Dirección después de girar
     */
    private DirectionEnum rotate(final InstructionEnum instructionRotate, final DirectionEnum originDirection) {
        return directionService.getDirection(instructionRotate, originDirection);
    }

    /**
     * Avanza una posición
     *
     * @param originDirection   Dirección del robot
     * @param originCoordenates Coordenadas antes de avanzar
     * @param limit             Limite del cuadrante para identificar si el robot se pierde
     * @return Nuevas coordenadas, incluso si el robot se pierde
     * @throws CoordinateInvalidException Excepción en caso de que el robot super los límites (se pierda)
     */
    private Coordenates stepForward(final DirectionEnum originDirection, final Coordenates originCoordenates, final Coordenates limit)
            throws CoordinateInvalidException {

        // Calcula las nuevas coordenadas
        Coordenates newCoordenates = getNewCoordenate(originDirection, originCoordenates);

        // Si las nuevas coordenadas estan fuera de rango
        if (directionService.isOutEdge(newCoordenates, limit)) {
            // Si las coordenadas fuera de rango no se han registrado lo agrega para futuros robots
            if (directionService.isLostCoordenate(originDirection, newCoordenates)) {
                throw new CoordinateInvalidException("Coordenadas fuera de rango");
            }
        } else {
            // Asigna las nuevas coordenadas si se encuentran dentro del limite
            originCoordenates.setX(newCoordenates.getX());
            originCoordenates.setY(newCoordenates.getY());
        }

        return originCoordenates;
    }

    /**
     * Calcula la nueva coordenada
     *
     * @param originDirection   Dirección del robot
     * @param originCoordenates Coordeadas antes de moverse
     * @return Coordenadas después de moverse
     */
    private Coordenates getNewCoordenate(final DirectionEnum originDirection, final Coordenates originCoordenates) {
        long newX = originCoordenates.getX();
        long newY = originCoordenates.getY();
        switch (originDirection) {
            case EAST:
                newX = originCoordenates.getX() + 1;
                break;
            case WEST:
                newX = originCoordenates.getX() - 1;
                break;
            case NORT:
                newY = originCoordenates.getY() + 1;
                break;
            case SOUTH:
                newY = originCoordenates.getY() - 1;
                break;
        }

        return new Coordenates(newX, newY);
    }
}
