package mx.challenge.draftea.martian.robots.exception;

/**
 * Excepcion cuando:
 * - La instruccion no es una de las esperadas
 * - Si el numero de instrucciones supera las definidas en el properties
 *
 * @author Edilberto Ventura
 */
public class InstructionInvalidException extends Exception {
    public InstructionInvalidException(final String message) {
        super(message);
    }
}
