package mx.challenge.draftea.martian.robots.util;

public abstract class ConstantsUtil {

    public static final String SPACE = " ";
    public static final String EMPTY = "";
    public static final String LOST = "LOST";

    public static final String MAX_VALUE_COORDENATE = "max.value.coordenate";
    public static final String MAX_SIZE_INSTRUCTIONS = "max.size.instructions";

    public static final String ERROR_TYPE_FILE = "Tipo de archivo no permitido";
    public static final String ERROR_COORDENATE_DIRECTION_NOT_FOUND = "No se especificaron las coordenadas y dirección correctamente";
    public static final String ERROR_COORDENATE_NOT_FOUND = "No se especificaron las coordenadas";
    public static final String ERROR_COORDENATE_INVALID = "Los valores de las coordenadas no son validas";
    public static final String ERROR_INSTRUCTIONS_LARGE = "Instrucción demasiado larga";
    public static final String ERROR_INSTRUCTIONS_INVALID = "Instrucción no válida";
}
