package mx.challenge.draftea.martian.robots.service;

import mx.challenge.draftea.martian.robots.model.Coordenates;
import mx.challenge.draftea.martian.robots.util.DirectionEnum;
import mx.challenge.draftea.martian.robots.util.InstructionEnum;

public interface IDirectionService {

    DirectionEnum getDirection(InstructionEnum instruction, DirectionEnum originDirection);

    void addLostDirection(DirectionEnum direction, Coordenates coordenates);

    boolean isOutEdge(final Coordenates coordenates, final Coordenates limit);

    boolean isLostCoordenate(DirectionEnum direction, Coordenates coordenates);

    void clearLostCoordenates();
}
