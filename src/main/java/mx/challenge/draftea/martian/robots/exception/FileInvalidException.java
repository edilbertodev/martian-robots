package mx.challenge.draftea.martian.robots.exception;

/**
 * Excepcion cuando:
 * - El archivo no es txt
 * - El archivo no tiene la estructura esperada, definida en las descripcion del problema
 *
 * @author Edilberto Ventura
 */
public class FileInvalidException extends Exception {

    public FileInvalidException(final String message) {
        super(message);
    }
}
