package mx.challenge.draftea.martian.robots.model;

/**
 * Modelo para trabajar las coordenadas del plano
 *
 * @author Edilberto Ventura
 */
public class Coordenates {
    private long x;

    private long y;

    public Coordenates() {
    }

    public Coordenates(long x, long y) {
        this.x = x;
        this.y = y;
    }

    public long getX() {
        return x;
    }

    public void setX(long x) {
        this.x = x;
    }

    public long getY() {
        return y;
    }

    public void setY(long y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Coordenates{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
