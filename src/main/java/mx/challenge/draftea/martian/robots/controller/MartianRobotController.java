package mx.challenge.draftea.martian.robots.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import mx.challenge.draftea.martian.robots.service.IMartianRobotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@ApiOperation(value = "Martian Robots", notes = "Martian Robots", tags = "Martian Robots Controller")
public class MartianRobotController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MartianRobotController.class);

    @Autowired
    private IMartianRobotService martianRobotService;

    @PostMapping(value = "/martian/robots")
    @ApiOperation(
            value = "Ejecuta la posición final de los robots siguiendo las instrucciones",
            notes = "Ejecuta la posición final de los robots siguiendo las instrucciones")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Caso exitoso, devulve las posiciones " +
                    "finales de los robots o LOST si sale fuera del cuadrante definido"),
            @ApiResponse(code = 500, message = "Error general")
    })
    public ResponseEntity<?> moveRobots(@RequestParam("file") MultipartFile file) {
        LOGGER.info("request: {}", file.getContentType());
        ResponseEntity response;
        try {
            List<String> finalPositions = martianRobotService.processInstructions(file);
            response = ResponseEntity.ok(finalPositions);
        } catch (Exception e) {
            response = ResponseEntity.badRequest().body(e.getMessage());
        }

        return response;
    }
}
